use database blog_db;

-- insert users into database

INSERT INTO users( email, password, date_time_created ) VALUES ("johnsmith@gmail.com", "passwordA", 2021-01-01 01:00:00);

INSERT INTO users( email, password, date_time_created ) VALUES ("juandelacruz@gmail.com", "passwordB", 2021-01-01 02:00:00);

INSERT INTO users( email, password, date_time_created ) VALUES ("janesmith@gmail.com", "passwordC", 2021-01-01 03:00:00);

INSERT INTO users( email, password, date_time_created ) VALUES ("mariadelacruz@gmail.com", "passwordD", 2021-01-01 04:00:00);

INSERT INTO users( email, password, date_time_created ) VALUES ("johndoe@gmail.com", "passwordE", 2021-01-01 05:00:00);

--insert records(post) to blog_db database

INSERT INTO posts( user_id, title, content,date_time_posted ) VALUES (1, "First Code", "Hello World!", 2021-01-02 01:00:00);

INSERT INTO posts( user_id, title, content,date_time_posted ) VALUES (1, "Second Code", "Hello Earth!", 2021-01-02 02:00:00);

INSERT INTO posts( user_id, title, content,date_time_posted ) VALUES (2, "Third Code", "Welcome to Mars!", 2021-01-02 03:00:00);

INSERT INTO posts( user_id, title, content,date_time_posted ) VALUES (4, "Fourth Code", "Bye Bye solar System!", 2021-01-02 04:00:00);

-- get all post with an Author ID of 1

select post from posts where user_id =1;

--Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's Id.

update posts set content = "Hello to the people of the earth" where content="Hello Earth!" and user_id=1;


--delete the user with an email of "johndoe@gmail.com"

delete from user where email= "johndoe@gmail.com";








